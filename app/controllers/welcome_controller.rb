class WelcomeController < ApplicationController
  def index
    @categories = current_user.categories.paginate(page: params[:page], per_page: 5).order('created_at DESC').includes(:bookmarks)
  end
end
