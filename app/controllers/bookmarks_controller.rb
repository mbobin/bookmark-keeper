require 'pismo'

class BookmarksController < ApplicationController
  before_action :set_bookmark, only: [:show, :edit, :update, :destroy]

  
  respond_to do |format|
    format.html
    format.js
  end
  # GET /bookmarks
  # GET /bookmarks.json
  def index
     if params[:search]
      @bookmarks = current_user.bookmarks.search(params[:search]).paginate(:page => params[:page], per_page: 15).order("created_at DESC").includes(:category)
    else
      @bookmarks = current_user.bookmarks.paginate(:page => params[:page], per_page: 15).order('created_at DESC').includes(:category)
     end
  end

  # GET /bookmarks/1
  # GET /bookmarks/1.json
  def show
  end

  # GET /bookmarks/new
  def new
    @bookmark = current_user.bookmarks.new(params.permit(:url))
  end

  # GET /bookmarks/1/edit
  def edit
  end

  # POST /bookmarks
  # POST /bookmarks.json
  def create
    @bookmark = current_user.bookmarks.new(bookmark_params)
    @bookmark.title = Pismo::Document.new(@bookmark.url).title

    respond_to do |format|
      if @bookmark.save
        format.html { redirect_to @bookmark, notice: 'Bookmark was successfully created.' }
        format.json { render action: 'show', status: :created, location: @bookmark }
      else
        format.html { render action: 'new' }
        format.json { render json: @bookmark.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bookmarks/1
  # PATCH/PUT /bookmarks/1.json
  def update
    respond_to do |format|
      if @bookmark.update(bookmark_params)
        format.html { redirect_to @bookmark, notice: "Bookmark was successfully updated. #{undo_link}"}
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @bookmark.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bookmarks/1
  # DELETE /bookmarks/1.json
  def destroy
    @bookmark.destroy
    redirect_to bookmarks_path, notice: "Bookmark was successfully deleted. #{undo_link}"
    #respond_to do |format|
    #  format.html { redirect_to bookmarks_url, notice: 'Bookmark was successfully deleted. #{undo_link}'}
    #  format.json { head :no_content }
    #end
  end



def undo_link
  view_context.link_to("undo", revert_version_path(@bookmark.versions.scoped.last), :method => :post)
end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bookmark
      @bookmark = current_user.bookmarks.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bookmark_params
      params.require(:bookmark).permit(:url, :description, :category_id)
    end
end
