class Category < ActiveRecord::Base
  has_many :bookmarks, dependent: :destroy
  belongs_to :user
  
  validates :name, length: { in: 3..50 }, presence: true
  validates :user_id, presence: true

end
