class Bookmark < ActiveRecord::Base
  belongs_to :category
  belongs_to :user

  has_paper_trail

  validates :user_id, presence: true
  validates :url, format: {with: Regexp.new(URI::regexp(%w(http https)))}, presence: true
  validates :category_id, presence: true
  
  def timestamp
    created_at.strftime('%d %B %Y %H:%M:%S')
  end
  
  def self.search(query)
    # where(:title, query) -> This would return an exact match of the query
    where("lower(title) like ? or lower(description) like ?", "%#{query.downcase}%", "%#{query.downcase}%") 
  end
end
