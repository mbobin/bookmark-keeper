# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user = User.create(name: 'mgb313', email: 'me@test.abc', password: 'abcdABCD',
        password_confirmation: 'abcdABCD')
user.confirm!
7.times do |c|
  Category.create(name: "Category #{c}", user_id: user.id)
  50.times do |i|
    Bookmark.create(title: "DevAcademy #{c}.#{i}", url: "http://www.devacademy.ro",
                      description: BetterLorem.p(1, true, false), category_id: c+1,
                      user_id: user.id) 
  end
end