ActionMailer::Base.smtp_settings = {
    :address   => "smtp.mandrillapp.com",
    :port      => 587,
    enable_strattls_auto: true,
    :user_name => ENV['MANDRILL_USERNAME'],
    :password  => ENV['MANDRILL_API_KEY'],
    authentication: 'login'

  }
ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.default_url_options = { host: 'http://bk.mbobin.me' }

MandrillMailer.configure do |config|
  config.api_key = ENV['MANDRILL_API_KEY']
end