# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
BookmarkKeeper::Application.config.secret_key_base = '0ab9b1c08bd06e62aceaac73d8cea2e4aed340f72c77ff42f964ba2f7d5eebdaab2df8b64d6e69bfaa12dec342703fea9fba64048dc6441537ed5d684ac7d909'
